import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.text.*;
import java.util.*;
import java.util.List; // resolves problem with java.awt.List and java.util.List
import java.io.File;
import java.lang.System;

/**
 * A class that represents a picture.  This class inherits from 
 * SimplePicture and allows the student to add functionality to
 * the Picture class.  
 * 
 * @author Barbara Ericson ericson@cc.gatech.edu
 */
public class Picture extends SimplePicture 
{
  ///////////////////// constructors //////////////////////////////////
  
  /**
   * Constructor that takes no arguments 
   */
  public Picture ()
  {
    /* not needed but use it to show students the implicit call to super()
     * child constructors always call a parent constructor 
     */
    super();  
  }
  
  /**
   * Constructor that takes a file name and creates the picture 
   * @param fileName the name of the file to create the picture from
   */
  public Picture(String fileName)
  {
    // let the parent class handle this fileName
    super(fileName);
  }
  
  /**
   * Constructor that takes the width and height
   * @param height the height of the desired picture
   * @param width the width of the desired picture
   */
  public Picture(int height, int width)
  {
    // let the parent class handle this width and height
    super(width,height);
  }
  
  /**
   * Constructor that takes a picture and creates a 
   * copy of that picture
   * @param copyPicture the picture to copy
   */
  public Picture(Picture copyPicture)
  {
    // let the parent class do the copy
    super(copyPicture);
  }
  
  /**
   * Constructor that takes a buffered image
   * @param image the buffered image to use
   */
  public Picture(BufferedImage image)
  {
    super(image);
  }
  
  ////////////////////// methods ///////////////////////////////////////
  
  /**
   * Method to return a string with information about this picture.
   * @return a string with information about the picture such as fileName,
   * height and width.
   */
  public String toString()
  {
    String output = "Picture, filename " + getFileName() + 
      " height " + getHeight() 
      + " width " + getWidth();
    return output;
    
  }
  
  /** Method to set the blue to 0 */
  public void zeroBlue()
  {
    Pixel[][] pixels = this.getPixels2D();
    for (Pixel[] rowArray : pixels)
    {
      for (Pixel pixelObj : rowArray)
      {
        pixelObj.setBlue(0);
      }
    }
  }
  
  public int getAverageBlue()
  {
    int average = 0;
    Pixel[][] pixels = this.getPixels2D();
    for (Pixel[] rowArray : pixels)
    {
      for (Pixel pixelObj : rowArray)
      {
        average += pixelObj.getBlue();
      }
    }
    return average/(pixels.length*pixels[0].length);
  }
  
  public int getAverageGreen()
  {
    int average = 0;
    Pixel[][] pixels = this.getPixels2D();
    for (Pixel[] rowArray : pixels)
    {
      for (Pixel pixelObj : rowArray)
      {
        average += pixelObj.getGreen();
      }
    }
    return average/(pixels.length*pixels[0].length);
  }
  
  public void zeroWater()
  {
    int averageBlue = getAverageBlue();
    int averageGreen = getAverageGreen();
    Pixel[][] pixels = this.getPixels2D();
    for (Pixel[] rowArray : pixels)
    {
      for (Pixel pixelObj : rowArray)
      {
        if (pixelObj.getBlue() < averageBlue + 10 || pixelObj.getGreen() > averageGreen + 10)
        {
            int average = (pixelObj.getRed() + pixelObj.getGreen() + pixelObj.getBlue()) / 3;
            pixelObj.setRed(average);
            pixelObj.setGreen(average);
            pixelObj.setBlue(average);
        }
      }
    }
  }
  
     public void mirrorVerticalRightToLeft()
     {
         Pixel[][] pixels = this.getPixels2D();
         Pixel leftPixel = null;
         Pixel rightPixel = null;
         int width = pixels[0].length;
         for (int row = 0; row < pixels.length; row++)
         {
             for (int col = 0; col < width / 2; col++)
             {
                 leftPixel = pixels[row][col];
                 rightPixel = pixels[row][width - 1 - col];
                 leftPixel.setColor(rightPixel.getColor());
             }
         }
     } 
     
 public void mirrorHorizontal()
 {
   Pixel[][] pixels = this.getPixels2D();
   Pixel bottomPixel = null;
   Pixel topPixel = null;
   int length = pixels.length;
   int width = pixels[0].length;
   for (int row = 0; row < length/2; row++)
   {
     for (int col = 0; col < width; col++)
     {
       topPixel = pixels[row][col];
       bottomPixel = pixels[length - row - 1][col];
       topPixel.setColor(bottomPixel.getColor());
     }
   }
 } 
 
 public void mirrorDiagonal()
 {
   Pixel[][] pixels = this.getPixels2D();
   Pixel firstPixel = null;
   Pixel secondPixel = null;
   int length = pixels.length;
   for (int row=0; row<length; row++)
   {
       for (int col=0; col<row; col++)
       {
           firstPixel = pixels[row][col];
           secondPixel = pixels[col][row];
           secondPixel.setColor(firstPixel.getColor());
       }
   }
 }
  
  /** Method that mirrors the picture around a 
    * vertical mirror in the center of the picture
    * from left to right */
  public void mirrorVertical()
  {
    Pixel[][] pixels = this.getPixels2D();
    Pixel leftPixel = null;
    Pixel rightPixel = null;
    int width = pixels[0].length;
    for (int row = 0; row < pixels.length; row++)
    {
      for (int col = 0; col < width / 2; col++)
      {
        leftPixel = pixels[row][col];
        rightPixel = pixels[row][width - 1 - col];
        rightPixel.setColor(leftPixel.getColor());
      }
    } 
  }
  
  /** Mirror just part of a picture of a temple */
  public void mirrorTemple()
  {
    int mirrorPoint = 276;
    Pixel leftPixel = null;
    Pixel rightPixel = null;
    int count = 0;
    Pixel[][] pixels = this.getPixels2D();
    
    // loop through the rows
    for (int row = 27; row < 97; row++)
    {
      // loop from 13 to just before the mirror point
      for (int col = 13; col < mirrorPoint; col++)
      {
        
        leftPixel = pixels[row][col];      
        rightPixel = pixels[row]                       
                         [mirrorPoint - col + mirrorPoint];
        rightPixel.setColor(leftPixel.getColor());
        count++;
      }
    }
    System.out.println("" + count);
  }
  
    public void mirrorArms()
  {
    int mirrorPoint = 190;
    Pixel topPixel = null;
    Pixel bottomPixel = null;
    Pixel[][] pixels = this.getPixels2D();
    
    // loop through the rows
    for (int row = 160; row < mirrorPoint; row++)
    {
      for (int col = 100; col < 300; col++)
      {
        topPixel = pixels[row][col];      
        bottomPixel = pixels[220-(row-160)][col];
        bottomPixel.setColor(topPixel.getColor());
      }
    }
  }
  
  public void mirrorGull()
  {
    int mirrorPoint = 330;
    Pixel firstPixel = null;
    Pixel secondPixel = null;
    Pixel[][] pixels = this.getPixels2D();
    
    // loop through the rows
    for (int row = 230; row < mirrorPoint; row++)
    {
      for (int col = 235; col < 350; col++)
      {
        firstPixel = pixels[row][col];      
        secondPixel = pixels[row-10][col-150];
        secondPixel.setColor(firstPixel.getColor());
      }
    }
  }
  
   public void copy(Picture fromPic, int startRow, int endRow, int startCol, int endCol)
   {
        Pixel fromPixel = null;
        Pixel toPixel = null;
        Pixel[][] toPixels = this.getPixels2D();
        Pixel[][] fromPixels = fromPic.getPixels2D();
        for (int fromRow = 0, toRow = startRow; fromRow < fromPixels.length && toRow < toPixels.length && toRow < endRow; fromRow++, toRow++)
        {
            for (int fromCol = 0, toCol = startCol; fromCol < fromPixels[0].length && toCol < endCol; fromCol++, toCol++)
            {
                fromPixel = fromPixels[fromRow][fromCol];
                toPixel = toPixels[toRow][toCol];
                toPixel.setColor(fromPixel.getColor());
            }
        }
   } 
  
  /** copy from the passed fromPic to the
    * specified startRow and startCol in the
    * current picture
    * @param fromPic the picture to copy from
    * @param startRow the start row to copy to
    * @param startCol the start col to copy to
    */
  public void copy(Picture fromPic, 
                 int startRow, int startCol)
  {
    Pixel fromPixel = null;
    Pixel toPixel = null;
    Pixel[][] toPixels = this.getPixels2D();
    Pixel[][] fromPixels = fromPic.getPixels2D();
    for (int fromRow = 0, toRow = startRow; 
         fromRow < fromPixels.length &&
         toRow < toPixels.length; 
         fromRow++, toRow++)
    {
      for (int fromCol = 0, toCol = startCol; 
           fromCol < fromPixels[0].length &&
           toCol < toPixels[0].length;  
           fromCol++, toCol++)
      {
        fromPixel = fromPixels[fromRow][fromCol];
        toPixel = toPixels[toRow][toCol];
        toPixel.setColor(fromPixel.getColor());
      }
    }   
  }

   public void grayscale()
 {
    Pixel[][] pixels = this.getPixels2D();
    for (Pixel[] rowArray : pixels)
    {
        for (Pixel pixelObj : rowArray)
        {
            int average = (pixelObj.getRed() + pixelObj.getGreen() + pixelObj.getBlue()) / 3;
            pixelObj.setRed(average);
            pixelObj.setGreen(average);
            pixelObj.setBlue(average);
        }
    }
 } 
 
  public void myCollage()
  {
      Picture pic1 = new Picture("wall.jpg");
      Picture pic2 = new Picture("barbaraS.jpg");
      Picture pic3 = new Picture("butterfly1.jpg");
      pic1.zeroBlue();
      pic2.grayscale();
      pic3.mirrorVertical();
      this.copy(pic1, 0, 700, 0, 700);
      this.copy(pic2, 200, 300, 0, 200);
      this.copy(pic3, 300, 400, 0, 200);
      this.mirrorVertical();
      this.mirrorHorizontal();
      this.write("mycollage.jpg");
  }
  
   public void edgeDetectionUpDown(int edgeDist)
 {
     Pixel leftPixel = null;
     Pixel rightPixel = null;
     Pixel bottomPixel = null;
     Pixel[][] pixels = this.getPixels2D();
     Color rightColor = null;
     Color bottomColor = null;
     for (int row = 0; row < pixels.length; row++)
     {
         for (int col = 0; col < pixels[0].length-1; col++)
         {
             leftPixel = pixels[row][col];
             rightPixel = pixels[row][col+1];
             if (row+1 != pixels.length)
             {
                bottomPixel = pixels[row+1][col];
                bottomColor = bottomPixel.getColor();
             }
             rightColor = rightPixel.getColor();
             
             if (leftPixel.colorDistance(rightColor) > edgeDist || leftPixel.colorDistance(bottomColor) > edgeDist)
                leftPixel.setColor(Color.BLACK);
             else
                leftPixel.setColor(Color.WHITE);
         }
     }
 } 
 
 public void newEdgeDetection(int edgeDist, int breadth) //for swan, (300, 250)
 {
     Pixel[][] pixels = this.getPixels2D();
     Color startColor = pixels[0][0].getColor();
     Color ideal = null;
     for (Pixel[] row : pixels)
     {
         for (Pixel col : row)
         {
             if (col.colorDistance(startColor) > edgeDist)
             {
                ideal = col.getColor();
                for (Pixel[] row2 : pixels)
                {
                    for (Pixel col2 : row2)
                    {
                        if (col2.colorDistance(ideal) > breadth)
                            col2.setColor(Color.WHITE);
                    }
                }
                return;
             }
         }
     }
 }
 
  /** Method to create a collage of several pictures */
  public void createCollage()
  {
    Picture flower1 = new Picture("flower1.jpg");
    Picture flower2 = new Picture("flower2.jpg");
    this.copy(flower1,0,50,0,50);
    this.copy(flower2,100,150,0,50);
    this.copy(flower1,200,50,0,50);
    Picture flowerNoBlue = new Picture(flower2);
    flowerNoBlue.zeroBlue();
    this.copy(flowerNoBlue,300,350,0,50);
    this.copy(flower1,400,450,0,50);
    this.copy(flower2,500,550,0,50);
    this.mirrorVertical();
    this.write("collage.jpg");
  }
  
  
  /** Method to show large changes in color 
    * @param edgeDist the distance for finding edges
    */
  public void edgeDetection(int edgeDist)
  {
    Pixel leftPixel = null;
    Pixel rightPixel = null;
    Pixel[][] pixels = this.getPixels2D();
    Color rightColor = null;
    for (int row = 0; row < pixels.length; row++)
    {
      for (int col = 0; 
           col < pixels[0].length-1; col++)
      {
        leftPixel = pixels[row][col];
        rightPixel = pixels[row][col+1];
        rightColor = rightPixel.getColor();
        if (leftPixel.colorDistance(rightColor) > 
            edgeDist)
          leftPixel.setColor(Color.BLACK);
        else
          leftPixel.setColor(Color.WHITE);
      }
    }
  }
  
  public boolean fridgeIsOpen()
 {
     Pixel[][] pixels = this.getPixels2D();
     long total = 0;
     int num = 0;
     for (Pixel[] row : pixels)
     {
         for (Pixel col : row)
         {
             total += (col.getBlue() + col.getRed() + col.getGreen())/3;
             num += 1;
         }
     }
     if (total / num > 25)
        return true;
     else
        return false;
 }
  
  /* Main method for testing - each class in Java can have a main 
   * method 
   */
  public static void main(String[] args) 
  {
      System.out.println("Starting........");
      String directoryPath = "D:/Jonathan_Rockett/Documents/Java/Personal Projects/pixLab/images";
      while(true)
      {
        File dir = new File(directoryPath);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
          for (File child : directoryListing) {
            String name = child.getName().toLowerCase();
            if (name.indexOf(".jpg") >= 0 || name.indexOf(".jpeg") > 0 || name.indexOf(".png") >= 0)
            {
                Picture pic = new Picture(child.getPath());
                if (pic.fridgeIsOpen() == false)
                  child.delete();
                else 
                  child.renameTo(new File(args[1] + child.getName()));
            }
          }
        } else {
        }
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() < startTime + 5000) {}
      }
  }
  
} // this } is the end of class Picture, put all new methods before this
